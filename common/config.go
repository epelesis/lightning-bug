package common

import (
	"bytes"
	"github.com/spf13/viper"
	"log"
)

func NewConfig() Config {
	conf := viper.New()
	conf.SetConfigName("config")
	conf.AddConfigPath("$HOME/lightning-bug")
	conf.AddConfigPath(".")
	err := conf.ReadInConfig()
	if err != nil {
		log.Panicln("could not load config:", err)
	}
	return &configImpl{conf}
}

// Returns a function that builds a config based on your input JSON payload. Used for unit testing and dig
func NewConfigBuilderFromJSON(payload string) func() Config {
	conf := viper.New()
	conf.SetConfigType("json")
	err := conf.ReadConfig(bytes.NewBuffer([]byte(payload)))
	if err != nil {
		log.Panicln("could not load config:", err)
	}
	return func() Config { return &configImpl{conf} }
}

type Config interface {
	GetDBFilename() string
	GetExecutorURL() string
}
type configImpl struct {
	conf *viper.Viper
}

func (c configImpl) GetDBFilename() string {
	return c.conf.GetString("db.filename")
}

func (c configImpl) GetExecutorURL() string {
	return c.conf.GetString("executor.url")
}
