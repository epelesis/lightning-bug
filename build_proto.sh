#!/usr/bin/env bash

set -x

# Generate GRPC Stub
#protoc -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis -I. \
#  --grpc-gateway_out=paths=source_relative:. \
protoc -I. \
  --go_out=paths=source_relative:. \
  --go-grpc_out=paths=source_relative:. \
  api/*.proto
