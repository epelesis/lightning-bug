package data

import (
	"nedellis.com/lightning-bug/common"
	"testing"

	"github.com/stretchr/testify/require"
)

// TODO: Break out into specific unit tests
func TestUser(t *testing.T) {
	db := NewDB(common.NewConfigBuilderFromJSON(`
{
  "db": {
    "filename": ":memory:"
  },
  "executor": {
    "url": "localhost:10101"
  }
}
`)()).(*dbImpl)

	expectedUser := User{Email: "a", Password: "b"}
	userID, err := db.CreateUser(expectedUser.Email, expectedUser.Password)
	if err != nil {
		t.Error(err)
	}
	createdUser, err := db.GetUserByEmail(expectedUser.Email)
	if err != nil {
		t.Error(err)
	}
	require.Equal(t, userID, createdUser.ID)
	require.Equal(t, expectedUser.Email, createdUser.Email)
	require.Equal(t, expectedUser.Password, createdUser.Password)

	createdUser, err = db.GetUserByID(createdUser.ID)
	if err != nil {
		t.Error(err)
	}
	require.Equal(t, expectedUser.Email, createdUser.Email)
	require.Equal(t, expectedUser.Password, createdUser.Password)

	err = db.DeleteUser(createdUser.ID)
	if err != nil {
		t.Error(err)
	}

	createdUser, err = db.GetUserByID(createdUser.ID)
	require.Equal(t, err.Error(), ErrorNoUser.Error())
}

// TODO: Break out into specific unit tests
func TestFunction(t *testing.T) {
	db := NewDB(common.NewConfigBuilderFromJSON(`
{
  "db": {
    "filename": ":memory:"
  },
  "executor": {
    "url": "localhost:10101"
  }
}
`)()).(*dbImpl)

	expectedUser := User{Email: "a", Password: "b"}
	userID, err := db.CreateUser(expectedUser.Email, expectedUser.Password)
	if err != nil {
		t.Error(err)
	}

	expectedFunction := Function{UserID: userID, Filename: "a", Contents: "b"}
	functionID, err := db.CreateFunction(expectedFunction.UserID, expectedFunction.Filename, expectedFunction.Contents)
	if err != nil {
		t.Error(err)
	}

	userFunctions, err := db.GetFunctionsByUser(userID)
	if err != nil {
		t.Error(err)
	}
	createdFunction := userFunctions[0]
	require.Equal(t, expectedFunction.UserID, createdFunction.UserID)
	require.Equal(t, expectedFunction.Filename, createdFunction.Filename)
	require.Equal(t, expectedFunction.Contents, createdFunction.Contents)
	require.Equal(t, functionID, createdFunction.ID)

	createdFunction, err = db.GetFunctionByID(createdFunction.ID)
	if err != nil {
		t.Error(err)
	}
	require.Equal(t, expectedFunction.UserID, createdFunction.UserID)
	require.Equal(t, expectedFunction.Filename, createdFunction.Filename)
	require.Equal(t, expectedFunction.Contents, createdFunction.Contents)

	err = db.DeleteFunction(createdFunction.ID)
	if err != nil {
		t.Error(err)
	}

	createdFunction, err = db.GetFunctionByID(createdFunction.ID)
	require.Equal(t, err.Error(), ErrorNoFunction.Error())
}

// TODO: Test Cascading Delete
