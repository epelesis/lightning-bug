package data

import (
	"nedellis.com/lightning-bug/api"
	"time"
)

type AutoIncr struct {
	ID      int64     `db:"id"`
	Created time.Time `db:"created"`
}

type User struct {
	AutoIncr
	Email    string `db:"email"`
	Password string `db:"password"`
}

type Function struct {
	AutoIncr
	UserID   int64  `db:"user_id"`
	Filename string `db:"filename"`
	Contents string `db:"contents"`
}

func UserDbToProto(user *User) *api.User {
	return &api.User{
		Id:       user.ID,
		Email:    user.Email,
		Password: user.Password,
	}
}

func UserProtoToDb(user *api.User) *User {
	return &User{
		AutoIncr: AutoIncr{ID: user.Id},
		Email:    user.Email,
		Password: user.Password,
	}
}

func FunctionDbToProto(function *Function) *api.Function {
	return &api.Function{
		Id:       function.ID,
		UserId:   function.UserID,
		Filename: function.Filename,
		Contents: function.Contents,
	}
}

func FunctionProtoToDb(function *api.Function) *Function {
	return &Function{
		AutoIncr: AutoIncr{ID: function.Id},
		UserID:   function.UserId,
		Filename: function.Filename,
		Contents: function.Contents,
	}
}

const schema = `
PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS users (
	id INTEGER PRIMARY KEY,
	created DATETIME DEFAULT CURRENT_TIMESTAMP,
	email TEXT NOT NULL UNIQUE,
	password TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS functions (
	id INTEGER PRIMARY KEY,
	created DATETIME DEFAULT CURRENT_TIMESTAMP,
	filename TEXT NOT NULL,
	contents TEXT NOT NULL,

	user_id INTEGER NOT NULL,
	FOREIGN KEY(user_id) REFERENCES functions(id) ON DELETE CASCADE
	UNIQUE(user_id, filename)
);
`
