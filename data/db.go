package data

import (
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"nedellis.com/lightning-bug/common"
)

var ErrorNoUser = errors.New("sql: no rows in result set")
var ErrorNoFunction = errors.New("sql: no rows in result set")

func NewDB(config common.Config) DB {
	db := sqlx.MustConnect("sqlite3", config.GetDBFilename())

	db.MustExec(schema)

	return &dbImpl{config: config, db: db}
}

type DB interface {
	CreateUser(email, password string) (int64, error)
	DeleteUser(userID int64) error
	GetUserByID(userID int64) (User, error)
	GetUserByEmail(email string) (User, error)

	CreateFunction(userID int64, filename, contents string) (int64, error)
	DeleteFunction(functionID int64) error
	GetFunctionByID(functionID int64) (Function, error)
	GetFunctionsByUser(userdD int64) ([]Function, error)
}

type dbImpl struct {
	config common.Config
	db     *sqlx.DB
}

func (d dbImpl) CreateUser(email, password string) (int64, error) {
	statement := "INSERT INTO users (email, password) VALUES (?, ?)"
	res, err := d.db.Exec(statement, email, password)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}

func (d dbImpl) DeleteUser(userID int64) error {
	statement := "DELETE FROM users WHERE id=?"
	_, err := d.db.Exec(statement, userID)
	return err
}

func (d dbImpl) GetUserByID(userID int64) (User, error) {
	statement := "SELECT * FROM users WHERE id=?"
	var user User
	err := d.db.Get(&user, statement, userID)
	return user, err
}

func (d dbImpl) GetUserByEmail(email string) (User, error) {
	statement := "SELECT * FROM users WHERE email=?"
	var user User
	err := d.db.Get(&user, statement, email)
	return user, err
}

func (d dbImpl) CreateFunction(userID int64, filename, contents string) (int64, error) {
	statement := "INSERT INTO functions (user_id, filename, contents) VALUES (?, ?, ?)"
	res, err := d.db.Exec(statement, userID, filename, contents)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}

func (d dbImpl) DeleteFunction(FunctionID int64) error {
	statement := "DELETE FROM functions WHERE id=?"
	_, err := d.db.Exec(statement, FunctionID)
	return err
}

func (d dbImpl) GetFunctionByID(functionID int64) (Function, error) {
	statement := "SELECT * FROM functions WHERE id=?"
	var function Function
	err := d.db.Get(&function, statement, functionID)
	return function, err
}

func (d dbImpl) GetFunctionsByUser(userID int64) ([]Function, error) {
	statement := "SELECT * FROM functions WHERE user_id=?"
	var functions []Function
	err := d.db.Select(&functions, statement, userID)
	return functions, err
}
