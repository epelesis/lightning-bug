package compiler

import (
	"fmt"
	"io/ioutil"
	"os/exec"
	"path/filepath"
	"sync"

	"nedellis.com/lightning-bug/data"
)

var mux sync.Mutex
var _ Compile = Zig

func NewCompilers() map[string]Compile {
	return map[string]Compile{
		".zig": Zig,
	}
}

type Compile func(data.Function) ([]byte, error)

func Zig(p data.Function) ([]byte, error) {
	mux.Lock()
	defer mux.Unlock()

	// 1. Write Program to Disk
	source := fmt.Sprintf("/tmp/program%s", filepath.Ext(p.Filename))
	dest := "/tmp/program.wasm"
	err := ioutil.WriteFile(source, []byte(p.Contents), 0644)
	if err != nil {
		return nil, err
	}

	// 2. Compile Program
	out, err := exec.Command("zig", "build-lib", source, "-target", "wasm32-wasi", "--release-small").CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("Failed to compile %s %w\n%s", source, err, out)
	}

	// 3. Copy WASM to Memory
	data, err := ioutil.ReadFile(dest)
	return data, err
}
