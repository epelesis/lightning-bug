.DEFAULT_GOAL := build

# OS dependent installers 
UNAME := $(shell uname)
UNAME := $(shell uname)
ifeq ($(UNAME), Linux)
	PROTO_INSTALL = apt-get update && apt-get install -y protobuf-compiler
endif
ifeq ($(UNAME), Darwin)
	PROTO_INSTALL = brew install protobuf
endif

.PHONY: setup
setup:
	go get github.com/golang/protobuf/protoc-gen-go
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go get -u golang.org/x/lint/golint
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	$(PROTO_INSTALL)

.PHONY: clean
clean: build
	go fmt ./...
	go clean
	go mod tidy
	rm -f $(shell cat config.json | jq .db.filename -r)
	git clean -fd
	git clean -fX

.PHONY: lint
lint:
	go vet ./...
	golint ./...

.PHONY: proto
proto:
	$(shell ./build_proto.sh)

.PHONY: build
build: proto
	go build

.PHONY: test
test: build lint
	go test -v ./...

.PHONY: image
image:
	docker build -t lightning-bug .
	echo "Run: docker run -it --rm lightning-bug:latest"
