# API Design Document

## [Design Flow](https://cloud.google.com/apis/design/resources#design_flow)
- Determine what types of resources an API provides.
- Determine the relationships between resources.
- Decide the resource name schemes based on types and relationships.
- Decide the resource schemas.
- Attach minimum set of methods to resources.

## Standard Methods
__List, Get, Create, Update, Delete__

| Method | HTTP | INPUT | OUTPUT |
- List	GET <collection URL>	N/A	Resource* list
- Get	GET <resource URL>	N/A	Resource*
- Create	POST <collection URL>	Resource	Resource*
- Update	PUT or PATCH <resource URL>	Resource	Resource*
- Delete	DELETE <resource URL>	N/A	google.protobuf.Empty**

## Resources

### User
- User: `/users/*/`
- Functions: `/users/*/functions/*`

### Function
