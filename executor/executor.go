package executor

import (
	"fmt"
	"path/filepath"

	"github.com/bytecodealliance/wasmtime-go"
	"nedellis.com/lightning-bug/common"
	"nedellis.com/lightning-bug/compiler"
	"nedellis.com/lightning-bug/data"
)

func NewExecutor(config common.Config, compilers map[string]compiler.Compile) Executor {
	return &executorImpl{config: config, compilers: compilers, store: wasmtime.NewStore(wasmtime.NewEngine())}
}

type Executor interface {
	Run(data.Function, string) (string, error)
}

type executorImpl struct {
	config    common.Config
	store     *wasmtime.Store
	compilers map[string]compiler.Compile
}

func (e *executorImpl) Run(p data.Function, req string) (string, error) {
	ext := filepath.Ext(p.Filename)

	compiler, exists := e.compilers[ext]
	if !exists {
		return "", fmt.Errorf("Ext %s not supported", ext)
	}
	contents, err := compiler(p)
	if err != nil {
		return "", fmt.Errorf("failed to compile: %w", err)
	}

	module, err := wasmtime.NewModule(e.store.Engine, contents)
	if err != nil {
		return "", fmt.Errorf("NewModule err: %w", err)
	}

	instance, err := wasmtime.NewInstance(e.store, module, []*wasmtime.Extern{})
	if err != nil {
		return "", fmt.Errorf("NewInstance err: %w", err)
	}

	receive := instance.GetExport("add").Func()
	res, err := receive.Call(1, 1)

	return fmt.Sprintf("%v", res), err
}
