FROM golang:1.15-buster

WORKDIR /
ADD . /

RUN make build

CMD ["/lightning-bug", "cli"]
