# Lightning Bug

[![pipeline status](https://gitlab.com/epelesis/lightning-bug/badges/master/pipeline.svg)](https://gitlab.com/epelesis/lightning-bug/-/commits/master)
[![go report](https://goreportcard.com/badge/gitlab.com/epelesis/lightning-bug)](https://goreportcard.com/report/gitlab.com/epelesis/lightning-bug)

Serverless Code Execution based on WASM/WASI

## Build
```
make setup
make
```

## Run
```
./lightning-bug server
```

## Supported Languages
- [Zig](https://ziglang.org)

## Architecture
Entrypoint: GRPC or [REST proxied by Envoy](https://www.envoyproxy.io/docs/envoy/latest/configuration/http/http_filters/grpc_json_transcoder_filter)

## Inspirations/Related
- [Olin](https://christine.website/blog/series/olin)
- [Repl.it](https://repl.it)
