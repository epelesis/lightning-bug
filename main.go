package main

import (
	"context"
	"log"

	"github.com/spf13/cobra"
	"go.uber.org/dig"
	"nedellis.com/lightning-bug/common"
	"nedellis.com/lightning-bug/compiler"
	"nedellis.com/lightning-bug/data"
	"nedellis.com/lightning-bug/executor"
	"nedellis.com/lightning-bug/server"
)

func main() {
	cmdServer := &cobra.Command{
		Use: "server",
		Run: func(cmd *cobra.Command, args []string) {
			container := dig.New()

			container.Provide(server.NewServer)
			container.Provide(common.NewConfig)
			container.Provide(data.NewDB)
			container.Provide(executor.NewExecutor)
			container.Provide(compiler.NewCompilers)

			err := container.Invoke(func(server server.Server) {
				server.Run(context.Background())
			})
			if err != nil {
				log.Panicln(err)
			}
		},
	}

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var rootCmd = &cobra.Command{}
	rootCmd.AddCommand(cmdServer)
	err := rootCmd.Execute()
	if err != nil {
		log.Panicln(err)
	}
}
