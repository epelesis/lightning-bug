package server

import (
	"context"
	"fmt"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
	"net"

	"google.golang.org/grpc"
	pb "nedellis.com/lightning-bug/api"
	"nedellis.com/lightning-bug/common"
	"nedellis.com/lightning-bug/data"
	"nedellis.com/lightning-bug/executor"
)

func NewServer(config common.Config, db data.DB, executor executor.Executor) Server {
	return Server{config, db, executor}
}

type Server struct {
	config   common.Config
	db       data.DB
	executor executor.Executor
}

func (s Server) CreateUser(ctx context.Context, req *pb.CreateUserRequest) (*pb.User, error) {
	id, err := s.db.CreateUser(req.User.Email, req.User.Password)
	if err != nil {
		return nil, err
	}
	return &pb.User{Id: id, Email: req.User.Email, Password: req.User.Password}, nil
}

func (s Server) GetUser(ctx context.Context, req *pb.GetUserRequest) (*pb.User, error) {
	switch key := req.Key.(type) {
	case *pb.GetUserRequest_Id:
		user, err := s.db.GetUserByID(key.Id)
		return data.UserDbToProto(&user), err
	}
	return nil, fmt.Errorf("could not match key: %s", req.Key)
}

func (s Server) DeleteUser(ctx context.Context, req *pb.DeleteUserRequest) (*emptypb.Empty, error) {
	return &emptypb.Empty{}, s.db.DeleteUser(req.Id)
}

func (s Server) CreateFunction(ctx context.Context, req *pb.CreateFunctionRequest) (*pb.Function, error) {
	id, err := s.db.CreateFunction(req.Function.UserId, req.Function.Filename, req.Function.Contents)
	if err != nil {
		return nil, err
	}
	return &pb.Function{Id: id, UserId: req.Function.UserId, Filename: req.Function.Filename, Contents: req.Function.Contents}, nil
}

func (s Server) GetFunction(ctx context.Context, req *pb.GetFunctionRequest) (*pb.Function, error) {
	switch key := req.Key.(type) {
	case *pb.GetFunctionRequest_Id:
		function, err := s.db.GetFunctionByID(key.Id)
		return data.FunctionDbToProto(&function), err
	}
	return nil, fmt.Errorf("could not match key: %s", req.Key)
}

func (s Server) DeleteFunction(ctx context.Context, req *pb.DeleteFunctionRequest) (*emptypb.Empty, error) {
	return &emptypb.Empty{}, s.db.DeleteFunction(req.Id)
}

func (s Server) InvokeFunction(ctx context.Context, req *pb.InvokeFunctionRequest) (*pb.InvokeFunctionResponse, error) {
	function, err := s.db.GetFunctionByID(req.Id)
	if err != nil {
		return nil, fmt.Errorf("Could not find function id=%d, %w", req.Id, err)
	}
	output, err := s.executor.Run(function, req.Payload)
	return &pb.InvokeFunctionResponse{Response: output}, err
}

// Run GRPC server, exits if context ends or the GRPC server encounters an error
func (s Server) Run(ctx context.Context) {
	lis, err := net.Listen("tcp", s.config.GetExecutorURL())
	if err != nil {
		log.Panicln(err)
	}
	defer lis.Close()
	server := grpc.NewServer()
	pb.RegisterUsersService(server, pb.NewUsersService(s))
	pb.RegisterFunctionsService(server, pb.NewFunctionsService(s))
	log.Printf("Listening on %s\n", s.config.GetExecutorURL())

	errChan := make(chan error)
	go func() {
		errChan <- server.Serve(lis)
	}()

	select {
	case err := <-errChan:
		log.Printf("Server died: %s", err)
	case <-ctx.Done():
		log.Printf("Server closed from context: %s", ctx.Err())
	}
}
