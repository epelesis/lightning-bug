package server

import (
	"context"
	"github.com/stretchr/testify/require"
	"go.uber.org/dig"
	"google.golang.org/grpc"
	"io/ioutil"
	"log"
	pb "nedellis.com/lightning-bug/api"
	"nedellis.com/lightning-bug/common"
	"nedellis.com/lightning-bug/compiler"
	"nedellis.com/lightning-bug/data"
	"nedellis.com/lightning-bug/executor"
	"testing"
)

func TestUser(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	container := dig.New()

	container.Provide(NewServer)
	container.Provide(common.NewConfigBuilderFromJSON(`
{
  "db": {
    "filename": ":memory:"
  },
  "executor": {
    "url": "localhost:10101"
  }
}
	`))
	container.Provide(data.NewDB)
	container.Provide(executor.NewExecutor)
	container.Provide(compiler.NewCompilers)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Launch In-Memory Server
	err := container.Invoke(func(server Server) {
		go server.Run(ctx)
	})
	if err != nil {
		t.Fatal(err)
	}

	// Setup a new client
	conn, err := grpc.Dial("localhost:10101", grpc.WithInsecure())
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()
	client := pb.NewUsersClient(conn)

	newUser, err := client.CreateUser(context.Background(), &pb.CreateUserRequest{
		User: &pb.User{Email: "example@example.com", Password: "12345"},
	})
	if err != nil {
		t.Fatal(err)
	}
	require.Equal(t, newUser.Email, "example@example.com")
	require.Equal(t, newUser.Password, "12345")

	foundUser, err := client.GetUser(context.Background(), &pb.GetUserRequest{
		Key: &pb.GetUserRequest_Id{Id: newUser.Id},
	})
	if err != nil {
		t.Fatal(err)
	}
	require.Equal(t, newUser.Id, foundUser.Id)
	require.Equal(t, newUser.Email, foundUser.Email)
	require.Equal(t, newUser.Password, foundUser.Password)

	_, err = client.DeleteUser(context.Background(), &pb.DeleteUserRequest{
		Id: newUser.Id,
	})
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.GetUser(context.Background(), &pb.GetUserRequest{
		Key: &pb.GetUserRequest_Id{Id: newUser.Id},
	})
	require.Contains(t, err.Error(), data.ErrorNoUser.Error())
}

func TestFunction(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	container := dig.New()

	container.Provide(NewServer)
	container.Provide(common.NewConfigBuilderFromJSON(`
{
  "db": {
    "filename": ":memory:"
  },
  "executor": {
    "url": "localhost:10102"
  }
}
	`))
	container.Provide(data.NewDB)
	container.Provide(executor.NewExecutor)
	container.Provide(compiler.NewCompilers)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Launch In-Memory Server
	err := container.Invoke(func(server Server) {
		go server.Run(ctx)
	})
	if err != nil {
		t.Fatal(err)
	}

	// Setup a new usersClient
	conn, err := grpc.Dial("localhost:10102", grpc.WithInsecure())
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()
	usersClient := pb.NewUsersClient(conn)
	functionClient := pb.NewFunctionsClient(conn)

	newUser, err := usersClient.CreateUser(context.Background(), &pb.CreateUserRequest{
		User: &pb.User{Email: "example@example.com", Password: "12345"},
	})
	if err != nil {
		t.Fatal(err)
	}

	contents, err := ioutil.ReadFile("../examples/identity.zig")
	if err != nil {
		t.Fatal(err)
	}

	newFunction, err := functionClient.CreateFunction(context.Background(), &pb.CreateFunctionRequest{
		Function: &pb.Function{
			UserId:   newUser.Id,
			Filename: "identity.zig",
			Contents: string(contents),
		},
	})
	require.Equal(t, newFunction.UserId, newUser.Id)
	require.Equal(t, newFunction.Filename, "identity.zig")
	require.Equal(t, newFunction.Contents, string(contents))

	foundFunction, err := functionClient.GetFunction(context.Background(), &pb.GetFunctionRequest{
		Key: &pb.GetFunctionRequest_Id{Id: newFunction.Id},
	})
	if err != nil {
		t.Fatal(err)
	}
	require.Equal(t, newFunction.Id, foundFunction.Id)
	require.Equal(t, newFunction.UserId, foundFunction.UserId)
	require.Equal(t, newFunction.Filename, foundFunction.Filename)
	require.Equal(t, newFunction.Contents, foundFunction.Contents)

	output, err := functionClient.InvokeFunction(context.Background(), &pb.InvokeFunctionRequest{
		Id:      newFunction.Id,
		Payload: `{"hello": "world"}`,
	})
	if err != nil {
		t.Fatal(err)
	}
	require.Equal(t, `{"hello": "world"}`, output.Response)

	_, err = functionClient.DeleteFunction(context.Background(), &pb.DeleteFunctionRequest{Id: newFunction.Id})
	if err != nil {
		t.Fatal(err)
	}

	_, err = functionClient.GetFunction(context.Background(), &pb.GetFunctionRequest{
		Key: &pb.GetFunctionRequest_Id{Id: newFunction.Id},
	})
	require.Contains(t, err.Error(), data.ErrorNoFunction.Error())
}
